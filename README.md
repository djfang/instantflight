![635573190313359845.png](https://bitbucket.org/repo/keB9oy/images/3101249536-635573190313359845.png)
![635573190805599623.png](https://bitbucket.org/repo/keB9oy/images/45599907-635573190805599623.png)
![635573190967995244.png](https://bitbucket.org/repo/keB9oy/images/2895789387-635573190967995244.png)

### Instant Flight Price Map  

#### Introduction
This **Instant Flight App** was inspired by planning last minute exploratory trips.  Oftentimes, due to busy work schedules, people do not have spare cycles to create trip plans. Since budgets have been a historically high deciding factor on the feasibility of select travel destinations, the ability to **INSTANTLY** align select travel destinations with budgets will make trip planning effortless. The purpose of this app is to do just that, to plan your travel **INSTANTLY**, by price.  

Although we have the traditional online travel agency such as Priceline, Expedia, and etc., none of them currently offer the option of viewing all the potential airport destinations from a given originating airport. However, Splunk’s ability to handle real-time and historical data can make this happen.  

As of now, the ability to see all destination airports on the map and color-coded by their flight ticket price is a notable accomplishment. This map was built using D3 and Topojson since the existing mapping tools were limiting from adopting directly. 

**USE CASE 1:** Airline company uses this app to gauge the market landscape and set their prices competitively.   
**USE CASE 2:** Travel agency can use this app to show its customers of all the potential flight options from an originating airport.. 

#### VISIO Diagram (Updated)
~~https://www.dropbox.com/s/yp5i68618jfmwh7/Visio-SplunkInstantFlight_DJFANG.pdf?dl=0~~
https://www.dropbox.com/s/n8xknizylruomqf/SplunkInstantFlight_DJFANG_Updated%20%2820150122%29.pdf?dl=0

#### Key Components
  - TA for handling airfare specific data  
    - This is done using a modified version of the JSON file from Google's QPXExpress API  
  - Splunk Django Binding App

#### Features (Some of the features that's working right now.)
  - Real time prices of all destination based on the origin show on maps  
  - Drill down panel to show all airports to choose as origin  
  - Map color changed based on price as heat map  
  - Lowest prices chart (Top 10)  
  - Highest price chart (Top 10)  
  - Delay Information
  - Historical Price Analysis

#### Installation
Installation is recommended to be performed via the command line, please refer to the VISIO diagram of the configurations.

### Demo Data and Extra info
Demo Generator data python scripts has been provided within the archive. The link to the demo data is : https://www.dropbox.com/s/mp489fayy3mrpcw/data.tar.gz?dl=0

Video: https://djfang.tinytake.com/sf/NDIyMjJfMzQxNzM0